module.exports = function(grunt) {

   // Project configuration.
   grunt.initConfig({
      // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),

      // Define reusable paths.
      paths: {
        app: 'app',
        dist: 'dist',
				app_content: '<%= paths.app %>/content',
        app_css: '<%= paths.app %>/css',
				app_fonts: '<%= paths.app %>/fonts',
				app_img: '<%= paths.app %>/img',
        app_js: '<%= paths.app %>/js',
        source_sass: '<%= paths.app %>/src/sass',

        dist_content: '<%= paths.dist %>/content',
				dist_css: '<%= paths.dist %>/css',
				dist_fonts: '<%= paths.dist %>/fonts',
				dist_img: '<%= paths.dist %>/img',
        dist_js: '<%= paths.dist %>/js'
      },

      sass: {
        dev: {
          options: {
            outputStyle: 'expanded',
            sourceMap: false
          },
          files: {
            '<%= paths.app_css %>/style.css' : '<%= paths.source_sass %>/style.scss'
          }
        },

        build: {
          options: {
            outputStyle: 'compressed',
            sourceMap: false
          },
          files: {
            '<%= paths.dist_css %>/style.css': '<%= paths.app_css %>/style.css'
          }
        }
      },

      browserSync: {
        bsFiles: {
           src: [
                 '<%= paths.app_css %>/*.css' ,
                 '<%= paths.app %>/*.html',
                 '<%= paths.app_js %>/*.js'
           ]
        },
        options: {
          browser: 'firefox',
          server: '<%= paths.app %>',
          watchTask: true
        }
      },

      watch: {
        sass: {
          files: ['<%= paths.source_sass %>/**/*.scss'],
          tasks: ['sass:dev']
        },
        js: {
          files: ['<%= paths.app_js %>/*.js'],
          tasks: []
        //  tasks: ['jshint']
        }
      },

      jshint: {
        dev: {
          files: {
            src: '<%= paths.app_js %>/*.js'
          }
        },
        options: {
          reporter: require('jshint-stylish')
        }
      },


      // Production Tasks
      copy: {
        html: {
          expand: true,
          cwd: '<%= paths.app %>',
          src: "*.html",
          dest: '<%= paths.dist %>/',
          options: {
            process: function(content, srcpath) {
              return content.replace('scripts.js','scripts.min.js');
            }
          }
        }

      },

      clean: {
        dist: {
          src: '<%= paths.dist %>'
        }
      },

      imagemin: {
        build: {
          files: [
             {
              expand: true,
              cwd: '<%= paths.app_img %>',
              src: ['**/*.{png,jpg,gif,svg,ico}'],
              dest: '<%= paths.dist_img %>'
             }
          ]
        }
      }



   });


      // Load the plugins.
      // ===========================================================================
      // LOAD GRUNT PLUGINS ========================================================
      // ===========================================================================
      // we can only load these if they are in our package.json
      // make sure you have run npm install so our app can find these
      grunt.loadNpmTasks('grunt-contrib-jshint');
      grunt.loadNpmTasks('grunt-contrib-uglify');
      grunt.loadNpmTasks('grunt-contrib-less');
      grunt.loadNpmTasks('grunt-contrib-cssmin');
      grunt.loadNpmTasks('grunt-contrib-watch');
      grunt.loadNpmTasks('grunt-contrib-concat');


      grunt.loadNpmTasks('grunt-contrib-copy');
      grunt.loadNpmTasks('grunt-contrib-clean');
      grunt.loadNpmTasks('grunt-contrib-imagemin');

      // BrowerserSync
      grunt.loadNpmTasks('grunt-browser-sync');
      grunt.loadNpmTasks('grunt-sass');
      grunt.loadNpmTasks('grunt-bower');

      // Create tasks.

      // Default task.
      grunt.registerTask('default', ['browserSync','watch']);

      // Build task.
      grunt.registerTask('build', ['clean:dist','copy','imagemin','uglify:build','concat:css','sass:build']);

};
